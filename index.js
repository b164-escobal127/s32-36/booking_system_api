const express = require("express");
const mongoose = require("mongoose")
require('dotenv').config();
const cors = require("cors");

const userRoutes = require("./routes/userRoutes")
const courseRoutes = require("./routes/courseRoutes")

const app = express();


//Allows all resources/origins to access our backend application
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended:true }));

//http://localhost:4000/api/users/register
app.use("/api/users", userRoutes);
app.use("/api/course", courseRoutes);

//Connect to out MongoDB connection
mongoose.connect(process.env.DB_CONNECTION, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', () => console.log('Now connected to MongoDB'));

app.listen(process.env.PORT, () => {
	console.log(`API is now online on port ${process.env.PORT}`)
});
