const Course = require("../model/Course");

//Create a new course
/*
Steps:
1. Create a new Course object
2. Save to the database
3. error handling
*/


module.exports.addCourse = (reqBody) => {
	console.log(reqBody);

	//Create a new object
	let newCourse = new Course({
		name: reqBody.course.name,
		description: reqBody.course.description,
		price: reqBody.course.price
	});

	return newCourse.save().then((course,error) => {
		if(error){
			return false;
		} else {
			return true
		}
	})
}


//retrieving all cources

module.exports.getAllCourses = () => {
	return Course.find({}).then( result => {
		return result
	})
}


//Retrieve all active courses

module.exports.getAllActive = () => {
	return Course.find({ isActive: true }).then(result => {
		return result;
	})
}


//retrieve specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams).then(result => {
		return result;
	})
}

//update a course
/*
Steps:
1. Create a variable which will contain the informatin retrieved from the request body
2. find and update course using the course ID
*/
module.exports.updateCourse = (courseId, reqBody) => {
	//specify the properties of the doc to be updated
	let updateCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	//findByIdAndUpdate(id, updatesToBeApplied)
	return Course.findByIdAndUpdate(courseId, updateCourse).then((course, err) => {
		//course not updated
		if(err) {
			return false;
		} else {
			//course updated successfully
			return true;
		}
	})

}


// activity 35

module.exports.archivedCourse=(reqParams, reqBody)=>{
	let updatedCourse = {
		isActive: reqBody.isActive
	} 

		return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((result, error)=>{
			if (error) {
				return false
			}else{
				return true
			}
	})

}













